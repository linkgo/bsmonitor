Sensor = function(index, name, unit, value, description, planIndexGroup) {
    this.index = index;
    this.name = name;
    this.unit = unit;
    this.value = value;
    this.description = description;
    this.planIndexGroup = planIndexGroup;
}

